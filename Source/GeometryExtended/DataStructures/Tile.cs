﻿namespace GeometryExtended.DataStructures
{
    using System.Drawing;

    public class Tile
    {
        public bool Empty { get; set; }

        public Pen Pen { get; set; }

        public Pen SidePen { get; set; }
    }
}
